package top.flash.test.singleton;

import top.flash.singleton.Singleton;
import top.flash.singleton.Singleton2;
import top.flash.singleton.Singleton3;

public class SingletonTest {
    public static void main(String[] args) {
        Singleton a1 = Singleton.getInstance();
        Singleton a2 = Singleton.getInstance();
        Singleton2 b1 = Singleton2.getInstance();
        Singleton2 b2 = Singleton2.getInstance();
        Singleton3 c1 = Singleton3.getSingleton();
        Singleton3 c2 = Singleton3.getSingleton();
        System.out.println(a1==a2);
        System.out.println(b1==b2);
        System.out.println(c1==c2);
    }
}
