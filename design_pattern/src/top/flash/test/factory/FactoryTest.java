package top.flash.test.factory;

import top.flash.factory.simple.Creator;
import top.flash.factory.simple.ProductA;
import top.flash.factory.simple.ProductB;

public class FactoryTest {

    public static void main(String[] args) {
        Creator creator = new Creator();
        ProductA productA = (ProductA) creator.factory("ProductA");
        ProductB productB = (ProductB) creator.factory("ProductB");
        productA.function();
        productB.function();
    }
}
