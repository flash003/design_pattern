package top.flash.factory.simple;

public class Creator {

    public IProduct factory(String className){
        if(className == null){
            return null;
        }
        if(className.equals("ProductA")){
            return new ProductA();
        }else if(className.equals("ProductB")){
            return new ProductB();
        }
        return null;
    }
}
