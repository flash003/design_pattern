package top.flash.singleton;

/**
 * 单例模式 -- 懒汉模式
 */
public class Singleton2 {

    //私有构造方法
    private Singleton2(){}

    //初始化时声明变量
    private  static Singleton2 instance;

    //提供外部获取实例方法
    public static Singleton2 getInstance(){
        if(null == instance){
            instance = new Singleton2();
        }
        return instance;
    }
}
