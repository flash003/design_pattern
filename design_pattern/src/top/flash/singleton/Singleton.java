package top.flash.singleton;

/**
 * 单例模式 -- 饿汉模式
 */
public class Singleton {

    //私有的构造方法
    private Singleton(){}

    //初始化时创建实例
    private static Singleton instance = new Singleton();

    //对外提供获取实例的方法
    public static Singleton getInstance(){
        return instance;
    }

}
