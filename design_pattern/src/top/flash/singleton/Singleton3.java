package top.flash.singleton;

/**
 * 双重校验锁
 */
public class Singleton3 {

    private volatile static Singleton3 instance;
    private Singleton3 (){}
    public static Singleton3 getSingleton() {
        if (instance == null) {
            synchronized (Singleton.class) {
                if (instance == null) {
                    instance = new Singleton3();
                }
            }
        }
        return instance;
    }
}
